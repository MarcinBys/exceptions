﻿namespace Wyjatki
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.textBoxData = new System.Windows.Forms.TextBox();
            this.exceptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.radioButtonWWII = new System.Windows.Forms.RadioButton();
            this.radioButtonWWI = new System.Windows.Forms.RadioButton();
            this.radioButtonFE = new System.Windows.Forms.RadioButton();
            this.radioButtonIOORE = new System.Windows.Forms.RadioButton();
            this.radioButtonNRE = new System.Windows.Forms.RadioButton();
            this.radioButtonDBZE = new System.Windows.Forms.RadioButton();
            this.dataGroupBox = new System.Windows.Forms.GroupBox();
            this.radioButtonInvalid = new System.Windows.Forms.RadioButton();
            this.radioButtonCorrect = new System.Windows.Forms.RadioButton();
            this.executeButton = new System.Windows.Forms.Button();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.exceptionsGroupBox.SuspendLayout();
            this.dataGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxCode
            // 
            this.textBoxCode.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCode.Location = new System.Drawing.Point(342, 28);
            this.textBoxCode.Multiline = true;
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxCode.Size = new System.Drawing.Size(614, 289);
            this.textBoxCode.TabIndex = 0;
            // 
            // textBoxData
            // 
            this.textBoxData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxData.Location = new System.Drawing.Point(12, 28);
            this.textBoxData.Multiline = true;
            this.textBoxData.Name = "textBoxData";
            this.textBoxData.Size = new System.Drawing.Size(324, 71);
            this.textBoxData.TabIndex = 1;
            // 
            // exceptionsGroupBox
            // 
            this.exceptionsGroupBox.Controls.Add(this.radioButtonWWII);
            this.exceptionsGroupBox.Controls.Add(this.radioButtonWWI);
            this.exceptionsGroupBox.Controls.Add(this.radioButtonFE);
            this.exceptionsGroupBox.Controls.Add(this.radioButtonIOORE);
            this.exceptionsGroupBox.Controls.Add(this.radioButtonNRE);
            this.exceptionsGroupBox.Controls.Add(this.radioButtonDBZE);
            this.exceptionsGroupBox.Location = new System.Drawing.Point(15, 120);
            this.exceptionsGroupBox.Name = "exceptionsGroupBox";
            this.exceptionsGroupBox.Size = new System.Drawing.Size(167, 160);
            this.exceptionsGroupBox.TabIndex = 8;
            this.exceptionsGroupBox.TabStop = false;
            this.exceptionsGroupBox.Text = "Wyjątki:";
            // 
            // radioButtonWWII
            // 
            this.radioButtonWWII.AutoSize = true;
            this.radioButtonWWII.Location = new System.Drawing.Point(6, 135);
            this.radioButtonWWII.Name = "radioButtonWWII";
            this.radioButtonWWII.Size = new System.Drawing.Size(117, 17);
            this.radioButtonWWII.TabIndex = 13;
            this.radioButtonWWII.TabStop = true;
            this.radioButtonWWII.Text = "Własny wyjątek #2";
            this.radioButtonWWII.UseVisualStyleBackColor = true;
            this.radioButtonWWII.CheckedChanged += new System.EventHandler(this.radioButtonWWII_CheckedChanged);
            // 
            // radioButtonWWI
            // 
            this.radioButtonWWI.AutoSize = true;
            this.radioButtonWWI.Location = new System.Drawing.Point(6, 112);
            this.radioButtonWWI.Name = "radioButtonWWI";
            this.radioButtonWWI.Size = new System.Drawing.Size(117, 17);
            this.radioButtonWWI.TabIndex = 12;
            this.radioButtonWWI.TabStop = true;
            this.radioButtonWWI.Text = "Własny wyjątek #1";
            this.radioButtonWWI.UseVisualStyleBackColor = true;
            this.radioButtonWWI.CheckedChanged += new System.EventHandler(this.radioButtonWWI_CheckedChanged);
            // 
            // radioButtonFE
            // 
            this.radioButtonFE.AutoSize = true;
            this.radioButtonFE.Location = new System.Drawing.Point(6, 88);
            this.radioButtonFE.Name = "radioButtonFE";
            this.radioButtonFE.Size = new System.Drawing.Size(104, 17);
            this.radioButtonFE.TabIndex = 11;
            this.radioButtonFE.TabStop = true;
            this.radioButtonFE.Text = "FormatException";
            this.radioButtonFE.UseVisualStyleBackColor = true;
            this.radioButtonFE.CheckedChanged += new System.EventHandler(this.radioButtonFE_CheckedChanged);
            // 
            // radioButtonIOORE
            // 
            this.radioButtonIOORE.AutoSize = true;
            this.radioButtonIOORE.Location = new System.Drawing.Point(6, 65);
            this.radioButtonIOORE.Name = "radioButtonIOORE";
            this.radioButtonIOORE.Size = new System.Drawing.Size(158, 17);
            this.radioButtonIOORE.TabIndex = 10;
            this.radioButtonIOORE.TabStop = true;
            this.radioButtonIOORE.Text = "IndexOutOfRangeException";
            this.radioButtonIOORE.UseVisualStyleBackColor = true;
            this.radioButtonIOORE.CheckedChanged += new System.EventHandler(this.radioButtonIOORE_CheckedChanged);
            // 
            // radioButtonNRE
            // 
            this.radioButtonNRE.AutoSize = true;
            this.radioButtonNRE.Location = new System.Drawing.Point(6, 42);
            this.radioButtonNRE.Name = "radioButtonNRE";
            this.radioButtonNRE.Size = new System.Drawing.Size(140, 17);
            this.radioButtonNRE.TabIndex = 9;
            this.radioButtonNRE.TabStop = true;
            this.radioButtonNRE.Text = "NullReferenceException";
            this.radioButtonNRE.UseVisualStyleBackColor = true;
            this.radioButtonNRE.CheckedChanged += new System.EventHandler(this.radioButtonNRE_CheckedChanged);
            // 
            // radioButtonDBZE
            // 
            this.radioButtonDBZE.AutoSize = true;
            this.radioButtonDBZE.Location = new System.Drawing.Point(6, 19);
            this.radioButtonDBZE.Name = "radioButtonDBZE";
            this.radioButtonDBZE.Size = new System.Drawing.Size(136, 17);
            this.radioButtonDBZE.TabIndex = 8;
            this.radioButtonDBZE.TabStop = true;
            this.radioButtonDBZE.Text = "DivideByZeroException";
            this.radioButtonDBZE.UseVisualStyleBackColor = true;
            this.radioButtonDBZE.CheckedChanged += new System.EventHandler(this.radioButtonDBZE_CheckedChanged);
            // 
            // dataGroupBox
            // 
            this.dataGroupBox.Controls.Add(this.radioButtonInvalid);
            this.dataGroupBox.Controls.Add(this.radioButtonCorrect);
            this.dataGroupBox.Location = new System.Drawing.Point(207, 120);
            this.dataGroupBox.Name = "dataGroupBox";
            this.dataGroupBox.Size = new System.Drawing.Size(105, 63);
            this.dataGroupBox.TabIndex = 9;
            this.dataGroupBox.TabStop = false;
            this.dataGroupBox.Text = "Dane";
            // 
            // radioButtonInvalid
            // 
            this.radioButtonInvalid.AutoSize = true;
            this.radioButtonInvalid.Location = new System.Drawing.Point(6, 43);
            this.radioButtonInvalid.Name = "radioButtonInvalid";
            this.radioButtonInvalid.Size = new System.Drawing.Size(60, 17);
            this.radioButtonInvalid.TabIndex = 1;
            this.radioButtonInvalid.TabStop = true;
            this.radioButtonInvalid.Text = "Błędne";
            this.radioButtonInvalid.UseVisualStyleBackColor = true;
            this.radioButtonInvalid.CheckedChanged += new System.EventHandler(this.radioButtonInvalid_CheckedChanged);
            // 
            // radioButtonCorrect
            // 
            this.radioButtonCorrect.AutoSize = true;
            this.radioButtonCorrect.Checked = true;
            this.radioButtonCorrect.Location = new System.Drawing.Point(6, 19);
            this.radioButtonCorrect.Name = "radioButtonCorrect";
            this.radioButtonCorrect.Size = new System.Drawing.Size(73, 17);
            this.radioButtonCorrect.TabIndex = 0;
            this.radioButtonCorrect.TabStop = true;
            this.radioButtonCorrect.Text = "Poprawne";
            this.radioButtonCorrect.UseVisualStyleBackColor = true;
            this.radioButtonCorrect.CheckedChanged += new System.EventHandler(this.radioButtonCorrect_CheckedChanged);
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(12, 301);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(300, 114);
            this.executeButton.TabIndex = 10;
            this.executeButton.Text = "Wykonaj";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxResult.Location = new System.Drawing.Point(342, 341);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(614, 74);
            this.textBoxResult.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(760, 427);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "© 2018 - Marcin Byś, Natalia Skopińska";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(339, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Wynik:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Dane:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(339, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Kod źródłowy:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 449);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.executeButton);
            this.Controls.Add(this.dataGroupBox);
            this.Controls.Add(this.exceptionsGroupBox);
            this.Controls.Add(this.textBoxData);
            this.Controls.Add(this.textBoxCode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Różne aspekty użycia obsługi wyjątków";
            this.exceptionsGroupBox.ResumeLayout(false);
            this.exceptionsGroupBox.PerformLayout();
            this.dataGroupBox.ResumeLayout(false);
            this.dataGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCode;
        private System.Windows.Forms.TextBox textBoxData;
        private System.Windows.Forms.GroupBox exceptionsGroupBox;
        private System.Windows.Forms.RadioButton radioButtonIOORE;
        private System.Windows.Forms.RadioButton radioButtonNRE;
        private System.Windows.Forms.RadioButton radioButtonDBZE;
        private System.Windows.Forms.GroupBox dataGroupBox;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.RadioButton radioButtonInvalid;
        private System.Windows.Forms.RadioButton radioButtonCorrect;
        private System.Windows.Forms.RadioButton radioButtonWWII;
        private System.Windows.Forms.RadioButton radioButtonWWI;
        private System.Windows.Forms.RadioButton radioButtonFE;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

