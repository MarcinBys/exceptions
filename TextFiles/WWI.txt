﻿namespace Wyjatki {
    class Program {
        static void Main(string[] args) {
            VerifyString(s)
        }

        private string VerifyString(string s) {
            try {
                if (s.Contains("ziemniak")) {
                    throw new Exception("Zdanie zawiera słowo \"ziemniak\"");
                }
                else {
                    return "Zdanie nie zawiera brzydkich słów.";
                }
            }
            catch (Exception e) {
                MessageBox.Show(e.ToString());
                return "Zdanie zawiera brzydkie słowo.";
            }
            finally {
                MessageBox.Show("Sprawdzanie zakończone.");
            }
        }
    }
}