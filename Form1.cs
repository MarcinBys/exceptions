﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wyjatki
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            if (radioButtonDBZE.Checked)
            {
                DivisionByZeroExample();
            }

            else if (radioButtonNRE.Checked)
            {
                NullReferenceExample();
            }

            else if (radioButtonIOORE.Checked)
            {
                IndexOutOfRangeExample();
            }

            else if (radioButtonFE.Checked)
            {
                FormatExample();
            }

            else if (radioButtonWWI.Checked)
            {
                WWIExample();
            }

            else if (radioButtonWWII.Checked)
            {
                WWIIExample();
            }
        }

        #region Division By Zero Exception
        private void radioButtonDBZE_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
            textBoxCode.Text = ReadFromFile("/TextFiles/DBZE.txt");
        }

        private int? DBZE(int a, int b)
        {
            try
            {
                return a / b;
            }
            catch (DivideByZeroException e)
            {
                MessageBox.Show(e.ToString().Substring(0, e.ToString().IndexOf(Environment.NewLine)));  //do wyswietlania pierwszej linii wiadomosci otrzymanego wyjatku
                return null;
            }
        }

        private void DivisionByZeroExample()
        {
            textBoxData.Text = "int a = 10" + Environment.NewLine;
            int a = 10;
            int b = 1;

            if (radioButtonCorrect.Checked)
            {
                textBoxData.Text += "int b = 5";
                b = 5;
            }
            else if (radioButtonInvalid.Checked)
            {
                textBoxData.Text += "int b = 0";
                b = 0;
            }

            textBoxResult.Text = DBZE(a, b).ToString();
        }
        #endregion

        #region Null Reference Exception
        private void radioButtonNRE_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
            textBoxCode.Text = ReadFromFile("/TextFiles/NRE.txt");
        }

        private string NRE(string s)
        {
            try
            {
                return s.ToUpper();
            }
            catch (NullReferenceException e)
            {
                MessageBox.Show(e.ToString().Substring(0, e.ToString().IndexOf(Environment.NewLine)));
                return null;
            }
        }

        private void NullReferenceExample()
        {
            string s = "";

            if (radioButtonCorrect.Checked)
            {
                textBoxData.Text = "string s = \"Hello world\"";
                s = "Hello world";
            }
            else if (radioButtonInvalid.Checked)
            {
                textBoxData.Text = "string s = null";
                s = null;
            }

            textBoxResult.Text = NRE(s);
        }
        #endregion

        #region Index Out Of Range Exception
        private void radioButtonIOORE_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
            textBoxCode.Text = ReadFromFile("/TextFiles/IOORE.txt");
        }

        private string IOORE(string[] array, int index)
        {
            try
            {
                return array[index];
            }
            catch (IndexOutOfRangeException e)
            {
                MessageBox.Show(e.ToString().Substring(0, e.ToString().IndexOf(Environment.NewLine)));
                return null;
            }
        }

        private void IndexOutOfRangeExample()
        {
            string[] array = new string[] { "zero", "jeden", "dwa", "trzy", "cztery", "piec" };
            int index = 0;

            textBoxData.Text = "string[] array = [\"" + string.Join("\", \"", array) + "\"]" + Environment.NewLine;

            if (radioButtonCorrect.Checked)
            {
                textBoxData.Text += "int index = 3";
                index = 3;
            }
            else if (radioButtonInvalid.Checked)
            {
                textBoxData.Text += "int index = 10";
                index = 10;
            }

            textBoxResult.Text = IOORE(array, index);
        }
        #endregion

        #region Format Exception
        private void radioButtonFE_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
            textBoxCode.Text = ReadFromFile("/TextFiles/FE.txt");
        }

        private int? FE(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch (FormatException e)
            {
                MessageBox.Show(e.ToString().Substring(0, e.ToString().IndexOf(Environment.NewLine)));
                return null;
            }
        }

        private void FormatExample()
        {
            string value = "";

            if (radioButtonCorrect.Checked)
            {
                textBoxData.Text = "string value = \"3\"";
                value = "3";
            }
            else if (radioButtonInvalid.Checked)
            {
                textBoxData.Text = "string value = \"trzy\"";
                value = "trzy";
            }

            textBoxResult.Text = FE(value).ToString();
        }
        #endregion

        #region Wlasny Wyjatek #1
        private void radioButtonWWI_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
            textBoxCode.Text = ReadFromFile("/TextFiles/WWI.txt");
        }

        private string WWI(string s)    //Verify Word
        {
            try
            {
                if (s.Contains("ziemniak"))
                {
                    throw new Exception("Zdanie zawiera słowo \"ziemniak\"");
                }
                else
                {
                    return "Zdanie nie zawiera brzydkich słów.";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString().Substring(0, e.ToString().IndexOf(Environment.NewLine)));
                return "Zdanie zawiera brzydkie słowo.";
            }
            finally
            {
                MessageBox.Show("Sprawdzanie zakończone.");
            }
        }

        private void WWIExample()
        {
            string s = "";

            if (radioButtonCorrect.Checked)
            {
                textBoxData.Text = "string s = \"Bardzo lubię jeść kukurydzę.\"";
                s = "Bardzo lubię jeść kukurydzę.";
            }
            else if (radioButtonInvalid.Checked)
            {
                textBoxData.Text = "string s = \"Bardzo lubię jeść ziemniaki.\"";
                s = "Bardzo lubię jeść ziemniaki.";
            }

            textBoxResult.Text = WWI(s);
        }
        #endregion

        #region Wlasny Wyjatek #2
        private void radioButtonWWII_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
            textBoxCode.Text = ReadFromFile("/TextFiles/WWII.txt");
        }

        private string WWII(int temperature)    //Verify Temperature
        {
            try
            {
                if (temperature < -100 || temperature > 100)
                {
                    throw (new OutdoorTemperatureOutOfRange());
                }
                else
                {
                    return "Podana temperatura jest poprawna.";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString().Substring(0, e.ToString().IndexOf(Environment.NewLine)));
                return "Podana temperatura jest niepoprawna.";
            }
        }

        private void WWIIExample()
        {
            int temperature = 0;

            if (radioButtonCorrect.Checked)
            {
                textBoxData.Text = "int temperature = 26";
                temperature = 26;
            }
            else if (radioButtonInvalid.Checked)
            {
                textBoxData.Text = "int temperature = -300";
                temperature = -300;
            }
            
            textBoxResult.Text = WWII(temperature);
        }
        #endregion

        private string ReadFromFile(string file)
        {
            TextReader tr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + file);
            string textFromFile = tr.ReadToEnd();
            return textFromFile;
        }

        private void radioButtonCorrect_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
        }

        private void radioButtonInvalid_CheckedChanged(object sender, EventArgs e)
        {
            textBoxData.Clear();
            textBoxResult.Clear();
        }
    }

    //Wlasny Wyjatek #2
    public class OutdoorTemperatureOutOfRange : Exception
    {
        public OutdoorTemperatureOutOfRange() : base(ShowException()) { }

        private static string ShowException()
        {
            return "Podana temperatura na dworze jest nieprawidłowa.";
        }
    }
}