﻿namespace Wyjatki {
    class Program {
        static void Main(string[] args) {
            Dzielenie(a, b)
        }

        private int? Dzielenie(int a, int b) {
            try {
                return a / b;
            }
            catch (DivideByZeroException e) {
                MessageBox.Show(e.ToString());
                return null;
            }
        }
    }
}